---
# https://taskfile.dev
#
# Taskfile.project.yml for your main project tasks. Must be commited.
# If you always want the last version of the task templates, add the following line in your .gitignore file
# /Taskfile.d/
#
version: '3'

vars:
  # TO MODIFY: Task templates to download separated by comma
  # Example: TASK_TEMPLATES: go,lint,yarn
  TASK_TEMPLATES: docker,git,lint,version
  DEFAULT_TAG: pre-commit

tasks:

  00-get-list-templates:
    # Get the list of templates to download
    # Do not remove
    cmds:
      - echo "{{.TASK_TEMPLATES}}"
    silent: true

  10-build-local:
    desc: "[PROJECT] Build an image locally. Arguments: [TAG|T=ansible:latest] [VCS_REF|C=110f273aad1cc] [FILE|F=<Dockerfile_path>] (*)"
    summary: |
      [PROJECT] Build an image locally.
      Usage: task 00:10-build-local [TAG|T=<image[:tag]>] [VCS_REF|C=<commit_sha>] [FILE|F=<Dockerfile_path>]

      Arguments:
       TAG     | T  Tag of the image (optional, default {{.FILE_TASK_START}})
       VCS_REF | C  Commit revision SHA hash (optional, by default NO_REF)
       FILE    | F  Dockerfile path (optional, by default Dockerfile)
    vars:
      TAG: '{{.TAG | default .T | default .DEFAULT_TAG}}'
      PRECOMMIT_VERSION:
        sh: task version:get-latest-pypi PACKAGE=pre-commit
      VCS_REF: '{{.VCS_REF | default .C | default "NO_REF"}}'
      FILE: '{{.FILE | default .F | default "Dockerfile"}}'
    cmds:
      - cp -f Taskfile.d/lint.yml .Taskfile-lint.yml
      - defer: rm -f .Taskfile-lint.yml
      - task docker:build-local TAG="{{.TAG}}" VERSION="{{.PRECOMMIT_VERSION}}" VCS_REF="{{.VCS_REF}}" FILE="{{.FILE}}"
    silent: true

  20-test:
    desc: "[PROJECT] Test an image locally. Arguments: [TAG|T=image:tag] (*)"
    summary: |
      [PROJECT] Test an image locally.
      Usage: task 00:30-test-local [TAG|T=<image[:tag]>]

      Arguments:
       TAG     | T  Tag of the image (optional)
    vars:
      TAG: '{{.TAG | default .T | default .DEFAULT_TAG}}'
    cmds:
      - docker run -t --rm {{.TAG}} /bin/sh -c "python --version"
      - docker run -t --rm {{.TAG}} /bin/sh -c "pre-commit --version"
      - docker run -t --rm -v $(pwd):/workdir {{.TAG}} /bin/bash -c "pre-commit run --all-files"
      - docker run -t --rm -v $(pwd):/workdir {{.TAG}} /bin/bash -c "task --version"
      - docker run -t --rm -v $(pwd):/workdir {{.TAG}} /bin/bash -c "task --taskfile /lint.yml pre-commit DIR=/workdir"
    silent: true

  30-pre-commit:
    desc: "[PROJECT] Pre-commit checks."
    cmds:
      - date > {{.FILE_TASK_START}}
      - defer: rm -f {{.FILE_TASK_START}}
      - task lint:pre-commit I=pre-commit P=n
      - task lint:all MEX='"#styles" "#Taskfile.d" "#.cache"'
      - task lint:docker
      - echo "" && echo "Checks Start $(cat {{.FILE_TASK_START}}) - End $(date)"
    silent: true
