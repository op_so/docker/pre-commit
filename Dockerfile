# hadolint ignore=DL3007
FROM python:3-slim-bookworm

ARG VERSION
ARG BUILD_DATE
ARG VCS_REF="DEV"

ENV container docker
ENV HOME /home/user
ENV PYTHONUNBUFFERED=1
ENV PIP_DISABLE_PIP_VERSION_CHECK=1
ENV PRE_COMMIT_HOME=/workdir/.cache/pre-commit

LABEL maintainer="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.title="pre-commit" \
    org.opencontainers.image.description="A lightweight docker image to lint code with pre-commit" \
    org.opencontainers.image.authors="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.version="${VERSION}" \
    org.opencontainers.image.url="https://hub.docker.com/r/jfxs/pre-commit" \
    org.opencontainers.image.source="https://gitlab.com/op_so/docker/pre-commit" \
    org.opencontainers.image.revision=${VCS_REF} \
    org.opencontainers.image.created=${BUILD_DATE}

# hadolint ignore=DL3008
RUN apt-get update && apt-get install -y --no-install-recommends \
       curl \
       git \
       wget \
    && pip3 install --no-cache-dir --upgrade \
       pre-commit==${VERSION} \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && sh -c "$(wget -qO - https://taskfile.dev/install.sh)" -- -d -b /usr/local/bin \
    && mkdir -p ${HOME} \
    && printf "[safe]\n  directory = \"*\"\n" >> ${HOME}/.gitconfig

COPY .Taskfile-lint.yml /lint.yml

WORKDIR /workdir

CMD ["pre-commit", "--version"]
