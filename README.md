<!-- vale off -->
# pre-commit
<!-- vale on -->

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=flat)](LICENSE)
[![Pipeline Status](https://gitlab.com/op_so/docker/pre-commit/badges/main/pipeline.svg)](https://gitlab.com/op_so/docker/pre-commit/pipelines)

A [Python](https://hub.docker.com/_/python) Docker image with [`pre-commit`](https://pre-commit.com/):

* **lightweight** image based on a debian-slim,
* `multiarch` with support of **amd64** and **arm64**,
* **automatically** updated by comparing software bill of materials (`SBOM`) changes,
* image **signed** with [Cosign](https://github.com/sigstore/cosign),
* a **software bill of materials (`SBOM`) attestation** added using [`Syft`](https://github.com/anchore/syft),
* available on **Docker Hub** and **Quay.io**.

[![GitLab](https://shields.io/badge/Gitlab-informational?logo=gitlab&style=flat-square)](https://gitlab.com/op_so/docker/pre-commit) The main repository.

[![Docker Hub](https://shields.io/badge/dockerhub-informational?logo=docker&logoColor=white&style=flat-square)](https://hub.docker.com/r/jfxs/pre-commit) The Docker Hub registry.

[![Quay.io](https://shields.io/badge/quay.io-informational?logo=docker&logoColor=white&style=flat-square)](https://quay.io/repository/ifxs/pre-commit) The Quay.io registry.

<!-- vale off -->
## Running pre-commit in local
<!-- vale on -->

```shell
docker run -t --rm -v $(pwd):/workdir jfxs/pre-commit /bin/bash -c "pre-commit run --all-files"
```

The first time `pre-commit` runs, it will automatically download, install, and run the necessary hooks (`pre-commit autoupdate`) and save them in the `.cache/pre-commit` directory.
Don't forget to add the `.cache` directory to the `.gitignore` file.

<!-- vale off -->
## Running pre-commit in local with [task](https://taskfile.dev/)
<!-- vale on -->

```shell
docker run -t --rm -v $(pwd):/workdir {{.TAG}} /bin/bash -c "task --taskfile /lint.yml pre-commit DIR=/workdir"
```

or with task installed locally and the lint [task template](https://gitlab.com/op_so/task/task-templates):

```shell
task lint:pre-commit
```

If the `.pre-commit-config.yaml` doesn't exist the following default file is created:

```yaml
---
repos:
  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v4.5.0
    hooks:
      - id: trailing-whitespace
      - id: end-of-file-fixer
      - id: check-yaml
      - id: check-added-large-files
      # Hooks added from sample-config
      - id: check-executables-have-shebangs
      - id: check-json
      - id: check-merge-conflict
      - id: check-shebang-scripts-are-executable
      - id: check-symlinks
      - id: check-toml
      - id: check-xml
      - id: detect-private-key
      - id: end-of-file-fixer
      - id: fix-byte-order-marker
      - id: mixed-line-ending
      - id: trailing-whitespace
        args: [--markdown-linebreak-ext=md]
```

<!-- vale off -->
## Running pre-commit in Gitlab-CI
<!-- vale on -->

Example of usage with Gitlab-CI:

```yaml
 ...
gitlab-release:
  image: jfxs/pre-commit
  stage: lint
  script:
    - task --taskfile /lint.yml pre-commit DIR=$(pwd)
```

## Built with

Docker latest tag is [--VERSION--](https://gitlab.com/op_so/docker/pre-commit/-/blob/main/Dockerfile) and has:

<!-- vale off -->
--SBOM-TABLE--
<!-- vale on -->

[`Dockerhub` Overview page](https://hub.docker.com/r/jfxs/pre-commit) has the details of the last published image.

## Versioning

Docker tag definition:

* the pre-commit version used,
* a dash
* an increment to differentiate build with the same version starting at 001

```text
<pre-commit_version>-<increment>
```

<!-- vale off -->
Example: 3.6.1-001
<!-- vale on -->

## Signature and attestation

[Cosign](https://github.com/sigstore/cosign) public key:

```shell
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEa3yV6+yd/l4zh/tfT6Tx+zn0dhy3
BhFqSad1norLeKSCN2MILv4fZ9GA6ODOlJOw+7vzUvzZVr9IXnxEdjoWJw==
-----END PUBLIC KEY-----
```

The public key is also available online: <https://gitlab.com/op_so/docker/cosign-public-key/-/raw/main/cosign.pub>.

To verify an image:

```shell
cosign verify --key cosign.pub $IMAGE_URI
```

To verify and get the software bill of materials (`SBOM`) attestation:

```shell
cosign verify-attestation --key cosign.pub --type spdxjson $IMAGE_URI | jq '.payload | @base64d | fromjson | .predicate'
```

## Authors

<!-- vale off -->
* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)
<!-- vale on -->

## License

<!-- vale off -->
This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
<!-- vale on -->
